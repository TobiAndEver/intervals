import { startApiCall, startApiCallRecursive } from "./index";

describe("poll", () => {
  it("calls an function in an intervall", async () => {
    jest.useFakeTimers();
    const functionToBeCalled = jest.fn();
    startApiCall(functionToBeCalled);

    jest.advanceTimersByTime(5000);

    expect(functionToBeCalled).toBeCalledTimes(5);
  });
  it("calls an async function in an intervall", async () => {
    jest.useFakeTimers();
    const functionToBeCalled = jest
      .fn()
      .mockImplementation(() => setTimeout(jest.fn(), 500));
    startApiCall(functionToBeCalled);

    jest.advanceTimersByTime(5000);

    expect(functionToBeCalled).toBeCalledTimes(5);
  });
  it("calls an async function which resolves after intervall has finished", async () => {
    jest.useFakeTimers();

    const timeOutFunction = jest.fn();
    const functionToBeCalled = jest
      .fn()
      .mockImplementation(() => setTimeout(timeOutFunction, 3000));
    startApiCall(functionToBeCalled);

    jest.advanceTimersByTime(5000);

    expect(timeOutFunction).toBeCalledTimes(2);
  });
});

describe("Intervall function with set timeout recursive", () => {
  it("calls an function in an intervall", async () => {
    jest.useFakeTimers();
    const functionToBeCalled = jest.fn();
    startApiCall(functionToBeCalled);

    jest.advanceTimersByTime(5000);

    expect(functionToBeCalled).toBeCalledTimes(5);
  });
  it("calls an async function in an intervall", async () => {
    jest.useFakeTimers();
    const functionToBeCalled = jest
      .fn()
      .mockImplementation(() => setTimeout(jest.fn(), 500));
    startApiCall(functionToBeCalled);

    jest.advanceTimersByTime(5000);

    expect(functionToBeCalled).toBeCalledTimes(5);
  });
  it("calls an async function which resolves after intervall has finished", async () => {
    jest.useFakeTimers();
    const functionFactoryToBeCalled = jest.fn().mockImplementation(() => wait(100));

    startApiCallRecursive(functionFactoryToBeCalled);
    jest.advanceTimersByTime(5000);
    expect(functionFactoryToBeCalled).toBeCalledTimes(5);
  });

  const wait = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));
});
