export const startApiCall = (fn: () => void) => {
  setInterval(fn, 1000);
};

export const startApiCallRecursive = (fn: () => Promise<void>) => {
  function timer() {
    setTimeout(async () => {
      const promise = fn();
      await promise;
      console.log("HIT1")
      const promise2 = fn();
      await promise2;
      console.log("HIT2")
      timer();
    }, 1000);
  }
  timer();
};

/* startApiCallRecursive(async () => {
  setTimeout(() => console.log("helo"), 5000);
});
 */
/* startApiCallRecursive(() => wait(2000));

function wait(ms: number): Promise<void> {
  console.log("hello");

  return new Promise((resolve) => setTimeout(resolve, ms));
} */
